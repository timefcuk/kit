import sqlite3
import datetime
import time
import subprocess
import os
import os.path
__name__ = "kitlib"

class TaskList:
	# folder, where we store database
	kit_folder = os.path.expanduser('~')+"/.kit/" 

	def __init__(self):
		# creates folder and database schema (tasks only) if they are not exist
		if (os.path.isfile(self.kit_folder+"kit.db")) == False:
			subprocess.run(["mkdir",self.kit_folder])
			subprocess.run(["touch",self.kit_folder+"kit.db"])
			con = sqlite3.connect(self.kit_folder+"kit.db")
			cur = con.cursor()
			cur.execute("CREATE TABLE Tasklist(tID INTEGER PRIMARY KEY AUTOINCREMENT,tName TEXT NOT NULL,tDescription TEXT,tDeadline DATETIME,tStart DATETIME,tFinish DATETIME,tCompletion BOOLEAN FALSE,tCreated DATETIME);")
			con.commit()
			cur.close()


	def createTask(self,tName, tDescription = None,tDeadline = None):
		con = sqlite3.connect(self.kit_folder+"kit.db")
		cur = con.cursor()
		cur.execute("INSERT INTO Tasklist(tCreated,tName,tDescription,tDeadline,tCompletion) SELECT datetime('now'),?,?,?,?",(tName,tDescription,tDeadline,False))
		con.commit()
		cur.close()
	
	def deleteTask(self,tID):
		con = sqlite3.connect(self.kit_folder+"kit.db")
		cur = con.cursor()
		cur.execute("DELETE FROM Tasklist WHERE tID = (?)",(tID,))
		con.commit()
		cur.close()

	def setTime(self,tID,tDeadline):
		con = sqlite3.connect(self.kit_folder+"kit.db")
		cur = con.cursor()
		cur.execute("UPDATE Tasklist SET tDeadline = (?) WHERE tID = (?)",(tDeadline,tID))
		con.commit()
		cur.close()

	def startTask(self,tID):
		con = sqlite3.connect(self.kit_folder+"kit.db")
		cur = con.cursor()
		cur.execute("UPDATE Tasklist SET tStart = datetime('now') WHERE tID = (?)",(tID,))
		con.commit()
		cur.close()

	def finishTask(self,tID):
		con = sqlite3.connect(self.kit_folder+"kit.db")
		cur = con.cursor()
		cur.execute("UPDATE Tasklist SET tFinish = datetime('now') WHERE tID = (?)",(tID,))
		cur.execute("UPDATE Tasklist SET tCompletion = (?) WHERE tID = (?)",(True,tID))
		con.commit()
		cur.close()

	def printALL(self):
		con = sqlite3.connect(self.kit_folder+"kit.db")
		cur = con.cursor()
		cur.execute("SELECT * FROM TaskList;")
		r = cur.fetchall()
		atts = ''
		for row in cur.description:
			atts+= row[0]+" | "
		print (atts)
		for row in r:
			print (row)
		cur.close()
