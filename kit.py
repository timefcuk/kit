import sqlite3
import datetime
import time
import subprocess
import os
import os.path
import kitlib
# to add new command, you have to add this command to command_list and to elif and to f_help!!

command_list = ['help','exit','create','print','delete','deadline','start','finish']

def in_clist(command):
	inlist = False
	for c in command_list:
		if c == command:
			inlist = True
	return inlist

def f_help():
	print ( "\"help\" to get this message \n"+
		"\"exit\" to quit the program\n"+
		"\"create\" to create a task\n"+
		"\"print\" to print all tasks\n"+
		"\"delete\" to delete task by its ID\n"+
		"\"deadline\" to set deadline on task by its ID\n"+
		"\"start\" to set task started (by ID)\n"+
		"\"finish\" to set task finished and completed (by ID)")

def error_msg():
	print ("Wrong command. Enter \"help\" to get a list of all availible commands, \"exit\" to quit")

def create_quiz():
	tName = input("-enter task name:")
	tDescription = input("-enter task description (optional)")
	if tDescription == '':
		tDescription = None
	if (input("-want to add deadline? (y/n)")=="y"):
		tyear,tmonth,tday,thour,tminutes,tseconds = map(int,input("--enter task deadline in format YYYY MM DD H M S (optional)").split())
		tDeadLine = datetime.datetime(tyear,tmonth,tday,thour,tminutes,tseconds)
	else:
		tDeadLine = None
	A = kitlib.TaskList()
	A.createTask(tName, tDescription, tDeadLine)
	print ("-Done!")



A = kitlib.TaskList()
pexit = False
print ("Hello, it's KIT. Enter \"help\" to get a list of all availible commands, \"exit\" to quit")
while pexit!=True:
	command = input()
	if in_clist(command)!=True:
		error_msg()
	else:
		if command == "help":
			f_help()
		elif command == "exit":
			exit()
		elif command == "create":
			create_quiz()		
		elif command == "print":
			A.printALL()
		elif command == "delete":
			A.deleteTask(input("-input task ID that you want to delete\n"))	
			print("-Done!")
		elif command == "deadline":
			tID = input("-input task ID\n")
			tyear,tmonth,tday,thour,tminutes,tseconds = map(int,input("--enter task deadline in format YYYY MM DD H M S\n").split())
			tDeadLine = datetime.datetime(tyear,tmonth,tday,thour,tminutes,tseconds)
			A.setTime(tID,tDeadLine)
			print("-Done!")
		elif command == "start":
			A.startTask(input("-input start ID\n"))
			print("-Done!")
		elif command == "finish":
			A.finishTask(input("-input start ID\n"))
			print("-Done!")
